//
//  aboutVC.swift
//  hinttell
//
//  Created by Mac Mini on 7/27/15.
//  Copyright (c) 2015 Mac Mini. All rights reserved.
//

import UIKit

class aboutVC: UIViewController {
    
    @IBOutlet weak var friendList: UIBarButtonItem!
    @IBOutlet weak var avatar: UIImageView!
    @IBOutlet weak var btnLeft: UIButton!
    @IBOutlet weak var btnRight: UIButton!
    @IBOutlet weak var segmented: UISegmentedControl!
    @IBOutlet weak var info: UIView!
    @IBOutlet weak var interest: UIView!
    @IBOutlet weak var product: UIView!
    @IBOutlet weak var friend: UIView!
    @IBAction func indexChanged(sender: UISegmentedControl) {
        switch segmented.selectedSegmentIndex
        {
        case 0:
            info.hidden = false
            interest.hidden = true
            product.hidden = true
            friend.hidden = true
        case 1:
            info.hidden = true
            interest.hidden = true
            product.hidden = false
            friend.hidden = true
        case 2:
            info.hidden = true
            interest.hidden = false
            product.hidden = true
            friend.hidden = true
        case 3:
            info.hidden = true
            interest.hidden = true
            product.hidden = true
            friend.hidden = false
        default:
            break;
        }

    }
    override func viewDidLoad() {
        super.viewDidLoad()

        avatar.layer.cornerRadius = avatar.frame.height/2
        avatar.clipsToBounds = true
        avatar.layer.borderWidth = 5
        avatar.layer.borderColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1).CGColor
        avatar.layer.masksToBounds = true
        btnLeft.layer.cornerRadius = btnLeft.frame.height/2
        btnRight.layer.cornerRadius = btnRight.frame.height/2
        
        info.hidden = false
        interest.hidden = true
        product.hidden = true
        friend.hidden = true
        
        if self.revealViewController() != nil {
            friendList.target = self.revealViewController()
            friendList.action = "revealToggle:"
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            self.revealViewController().rearViewRevealWidth = 325
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
