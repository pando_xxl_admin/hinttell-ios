//
//  searchFriend.swift
//  hinttell
//
//  Created by Mac Mini on 7/27/15.
//  Copyright (c) 2015 Mac Mini. All rights reserved.
//

import UIKit

class searchFriend: UIViewController, SMSegmentViewDelegate  {
    var segmentView: SMSegmentView!
    var margin: CGFloat = 10.0
    override func viewDidLoad() {
        super.viewDidLoad()
        var searchBar : UISearchBar = UISearchBar()
        searchBar.frame = CGRectMake(0, 20, self.view.frame.size.width - 50, 35)
        searchBar.layer.borderColor = UIColor(red: 0, green: 58/255, blue: 96/255, alpha: 1).CGColor
        searchBar.layer.borderWidth = 1.0
        //searchBar.placeholder = "Search"
        searchBar.barTintColor = UIColor(red: 0, green: 58/255, blue: 96/255, alpha: 1)
        let bgsearch = UIImage(named:"bgSearch")
        searchBar.setSearchFieldBackgroundImage(bgsearch, forState: UIControlState.Normal)
        searchBar.layer.cornerRadius = 5
        searchBar.clipsToBounds = true
        var searchTextField: UITextField? = searchBar.valueForKey("searchField") as? UITextField
        if searchTextField!.respondsToSelector(Selector("attributedPlaceholder")) {
            var color = UIColor.whiteColor()
            let attributeDict = [NSForegroundColorAttributeName: UIColor.whiteColor()]
            searchTextField!.attributedPlaceholder = NSAttributedString(string: "Search", attributes: attributeDict)
            searchTextField?.textColor = UIColor.whiteColor()
        }
        self.view.addSubview(searchBar)
        
        /*let barbuttonFont = UIFont(name: "ProximaNovaCond-Regular", size: 15) ?? UIFont.systemFontOfSize(15)
        segmented.setTitleTextAttributes([NSFontAttributeName: barbuttonFont, NSForegroundColorAttributeName:UIColor.whiteColor()], forState: UIControlState.Normal)
        segmented.setBackgroundImage(UIImage(named: "bgSegment"), forState: UIControlState.Normal, barMetrics: UIBarMetrics.Default)
*/
        self.segmentView = SMSegmentView(frame: CGRect(x: 10, y: 60, width: self.view.frame.size.width - 70, height: 40), separatorColour: UIColor.clearColor(), separatorWidth: 0.5, segmentProperties: [keySegmentTitleFont: UIFont(name: "ProximaNovaCond-Regular", size: 15)!, keySegmentOnSelectionColour: UIColor.clearColor(), keySegmentOffSelectionColour: UIColor.clearColor(), keyContentVerticalMargin: Float(10.0), keySegmentOnSelectionTextColour: UIColor.whiteColor(), keySegmentOffSelectionTextColour: UIColor(red: 94/255, green: 140/255, blue: 173/255, alpha: 1)])
        
        self.segmentView.delegate = self
        self.segmentView.layer.borderColor = UIColor.clearColor().CGColor
        self.segmentView.layer.borderWidth = 1.0
        
        
        // Add segments
        self.segmentView.addSegmentWithTitle("My friends", onSelectionImage: nil, offSelectionImage: nil)
        self.segmentView.addSegmentWithTitle("Mutual friends", onSelectionImage: nil, offSelectionImage: nil)
        self.segmentView.addSegmentWithTitle("Following", onSelectionImage: nil, offSelectionImage: nil)
        
        // Set segment with index 0 as selected by default
        segmentView.selectSegmentAtIndex(0)
        
        self.view.addSubview(self.segmentView)
    }

    func segmentView(segmentView: SMSegmentView, didSelectSegmentAtIndex index: Int) {
        /*
        Replace the following line to implement what you want the app to do after the segment gets tapped.
        */
        println("Select segment at index: \(index)")
    }
    
    override func supportedInterfaceOrientations() -> Int {
        return Int(UIInterfaceOrientationMask.All.rawValue)
    }
    
    override func willAnimateRotationToInterfaceOrientation(toInterfaceOrientation: UIInterfaceOrientation, duration: NSTimeInterval) {
        /*
        MARK: Replace the following line to your own frame setting for segmentView.
        */
        if toInterfaceOrientation == UIInterfaceOrientation.LandscapeLeft || toInterfaceOrientation == UIInterfaceOrientation.LandscapeRight {
            //self.segmentView.organiseMode = .SegmentOrganiseVertical
            //self.segmentView.segmentVerticalMargin = 25.0
            //self.segmentView.frame = CGRect(x: self.view.frame.size.width/2 - 40.0, y: 100.0, width: 80.0, height: 220.0)
        }
        else {
            //self.segmentView.organiseMode = .SegmentOrganiseHorizontal
            //self.segmentView.segmentVerticalMargin = 10.0
            //self.segmentView.frame = CGRect(x: self.margin, y: 120.0, width: self.view.frame.size.width - self.margin*2, height: 40.0)
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
