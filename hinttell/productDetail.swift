//
//  productDetail.swift
//  hinttell
//
//  Created by Mac Mini on 7/23/15.
//  Copyright (c) 2015 Mac Mini. All rights reserved.
//

import UIKit

class productDetail: UIViewController {

    @IBOutlet weak var segmented: UISegmentedControl!
    @IBOutlet weak var avatarSaler: UIImageView!
    
    @IBOutlet weak var viewComments: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var viewDes: UIView!
    @IBOutlet weak var viewFeedback: UIView!
    @IBAction func indexChanged(sender: UISegmentedControl) {
        switch segmented.selectedSegmentIndex
        {
        case 0:
            println("comments")
            viewComments.hidden = false
            viewDes.hidden = true
            viewFeedback.hidden = true
        case 1:
            println("description")
            viewComments.hidden = true
            viewDes.hidden = false
            viewFeedback.hidden = true
        case 2:
            println("feedback")
            viewComments.hidden = true
            viewDes.hidden = true
            viewFeedback.hidden = false
        default:
            break;
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        viewComments.hidden = false
        viewDes.hidden = true
        viewFeedback.hidden = true
        scrollView.contentSize = CGSizeMake(375, 1000)
        scrollView.showsHorizontalScrollIndicator = true
        scrollView.scrollEnabled = true
        avatarSaler.layer.cornerRadius = avatarSaler.frame.height/2
        avatarSaler.clipsToBounds = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
