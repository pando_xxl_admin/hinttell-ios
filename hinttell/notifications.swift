//
//  notifications.swift
//  hinttell
//
//  Created by Mac Mini on 7/23/15.
//  Copyright (c) 2015 Mac Mini. All rights reserved.
//

import UIKit

class notifications: UITableViewController, UIPopoverPresentationControllerDelegate  {

    @IBOutlet weak var friendList: UIBarButtonItem!
    @IBOutlet weak var avatar: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()

        var image = UIImage(named: "logotext20")
        self.navigationItem.titleView = UIImageView(image: image)
        if self.revealViewController() != nil {
            friendList.target = self.revealViewController()
            friendList.action = "revealToggle:"
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            self.revealViewController().rearViewRevealWidth = 325
        }
        avatar.layer.cornerRadius = avatar.frame.height/2
        avatar.clipsToBounds = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return 2
    }
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "popoverSegue" {
            println("show popover")
            let popoverViewController = segue.destinationViewController as! UIViewController
            popoverViewController.modalPresentationStyle = .Popover
            popoverViewController.preferredContentSize = CGSizeMake(375,300)
            popoverViewController.popoverPresentationController!.delegate = self
            //popoverViewController.popoverPresentationController?.permittedArrowDirections = .Any
            
        }
    }
    func adaptivePresentationStyleForPresentationController(controller: UIPresentationController) -> UIModalPresentationStyle {return UIModalPresentationStyle.None}

}
