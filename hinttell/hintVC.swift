//
//  hintVC.swift
//  hinttell
//
//  Created by Mac Mini on 7/27/15.
//  Copyright (c) 2015 Mac Mini. All rights reserved.
//

import UIKit
import MobileCoreServices

class hintVC: UIViewController, UIPopoverPresentationControllerDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate  {

    @IBOutlet weak var friendList: UIBarButtonItem!
    @IBOutlet weak var imgProduct: UIImageView!
    @IBAction func addImg(sender: UIButton) {
        let imagePicker = UIImagePickerController()
        imagePicker.editing = false
        imagePicker.delegate = self
        imagePicker.mediaTypes = [kUTTypeImage]
        imagePicker.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
        self.presentViewController(imagePicker, animated: true, completion: nil)
    }
    
    @IBOutlet weak var txtTag: UITextField!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtPrice: UITextField!
    @IBOutlet weak var tvDescription: UITextView!
    @IBOutlet weak var btnPrice: UIButton!
    @IBOutlet weak var btnPublic: UIButton!
    @IBOutlet weak var viewTag: UIView!
    @IBAction func btnTagFriend(sender: UIButton) {
        viewTag.hidden = false
    }
    @IBOutlet weak var showTag: UIView!
    @IBAction func editTag(sender: AnyObject) {
        showTag.hidden = false
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if self.revealViewController() != nil {
            friendList.target = self.revealViewController()
            friendList.action = "revealToggle:"
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            self.revealViewController().rearViewRevealWidth = 325
        }
        
        imgProduct.layer.borderWidth = 3
        imgProduct.layer.borderColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1).CGColor
        imgProduct.layer.masksToBounds = true
        
        btnPrice.setTitle("VND", forState: UIControlState.Normal)
        let imagePrice = UIImage(named: "arrow15") as UIImage!
        btnPrice.setImage(imagePrice, forState: UIControlState.Normal)
        btnPrice.imageEdgeInsets = UIEdgeInsetsMake(0, btnPrice.frame.size.width - imagePrice.size.width, 0, 0)
        //btnHint.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, image.size.width)
        btnPrice.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, imagePrice.size.width + 10)
        
        btnPublic.setTitle("Public", forState: UIControlState.Normal)
        let image = UIImage(named: "arrow15") as UIImage!
        btnPublic.setImage(image, forState: UIControlState.Normal)
        btnPublic.imageEdgeInsets = UIEdgeInsetsMake(0, btnPublic.frame.size.width - image.size.width, 0, 0)
        //btnHint.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, image.size.width)
        btnPublic.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, image.size.width + 10)
        
        let padding = UIView(frame:CGRectMake(0,0,10,self.txtName.frame.height))
        txtName.leftView=padding
        txtName.leftViewMode = UITextFieldViewMode.Always
        let borderF = CALayer()
        let widthF = CGFloat(1.0)
        borderF.borderColor = UIColor(red: 242/255, green: 242/255, blue: 242/255, alpha: 0.5).CGColor
        borderF.frame = CGRect(x: 0, y: txtName.frame.size.height - widthF, width:  txtName.frame.size.width, height: txtName.frame.size.height)
        borderF.borderWidth = widthF
        txtName.layer.addSublayer(borderF)
        txtName.layer.masksToBounds = true
        
        let paddingPrice = UIView(frame:CGRectMake(0,0,10,self.txtPrice.frame.height))
        txtPrice.leftView=paddingPrice
        txtPrice.leftViewMode = UITextFieldViewMode.Always
        let borderP = CALayer()
        let widthP = CGFloat(1.0)
        borderP.borderColor = UIColor(red: 242/255, green: 242/255, blue: 242/255, alpha: 0.5).CGColor
        borderP.frame = CGRect(x: 0, y: txtPrice.frame.size.height - widthP, width:  txtPrice.frame.size.width, height: txtPrice.frame.size.height)
        borderP.borderWidth = widthP
        txtPrice.layer.addSublayer(borderP)
        txtPrice.layer.masksToBounds = true
        
        let paddingTag = UIView(frame:CGRectMake(0,0,10,self.txtTag.frame.height))
        txtTag.leftView=paddingTag
        txtTag.leftViewMode = UITextFieldViewMode.Always

    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage!, editingInfo: [NSObject : AnyObject]!){
        
        //let image = editingInfo[UIImagePickerControllerOriginalImage] as! UIImage
        self.imgProduct.image = image
        println("image picker")
        self.dismissViewControllerAnimated(true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showPopover" {
            println("show popover")
            let popoverViewController = segue.destinationViewController as! UIViewController
            popoverViewController.modalPresentationStyle = .Popover
            popoverViewController.preferredContentSize = CGSizeMake(200,170)
            popoverViewController.popoverPresentationController!.delegate = self
            //popoverViewController.popoverPresentationController?.sourceRect = CGRectMake(100,0,100,0)
            //popoverViewController.popoverPresentationController?.permittedArrowDirections = .Any
            
            
        }
        if segue.identifier == "showPrice" {
            println("show popover price")
            let popoverViewController = segue.destinationViewController as! UIViewController
            popoverViewController.modalPresentationStyle = .Popover
            popoverViewController.preferredContentSize = CGSizeMake(100,130)
            popoverViewController.popoverPresentationController!.delegate = self
            //popoverViewController.popoverPresentationController?.sourceRect = CGRectMake(100,0,100,0)
            //popoverViewController.popoverPresentationController?.permittedArrowDirections = .Any
            
            
        }
        
    }
    func adaptivePresentationStyleForPresentationController(controller: UIPresentationController) -> UIModalPresentationStyle {return UIModalPresentationStyle.None}


}
